<?php

namespace App\Services;

use Symfony\Component\HttpKernel\Event\ResponseEvent;


class KernelResponseListener
{


    public function onKernelResponse(ResponseEvent $event)
    {

        $response = $event->getResponse();
        
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'DNT, X-User-Token, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Authorization');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Max-Age', 60 * 660 * 24);
        $response->setStatusCode(200);
        
        return;
        
    }
}
