<?php

namespace App\Repository;

use App\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<File>
 *
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function findByTag($value) : array 
    {

        return $this->getEntityManager()
        ->createQuery(
            'SELECT f.name, f.tag, IDENTITY(f.owner) FROM App\Entity\File f
             WHERE f.tag = :val'
        )
        ->setParameter('val', $value)
        ->getResult();
        
    }

    public function findAllNamesAndTags() : array 
    {
        return $this->getEntityManager()
        ->createQuery(
          'SELECT f.name, f.tag, IDENTITY(f.owner) FROM App\Entity\File f'
      )
      ->getResult();
    
    }

    // get the files which belong to one user identified by id 
    public function findByUserId($val) : array {
        return $this->getEntityManager()
        ->createQuery(
            'SELECT f.name, f.tag FROM App\Entity\File f
           WHERE IDENTITY(f.owner) = :val'
        )
        ->setParameter('val', $val) 
        ->getResult();
        
    }

    /* public function findOneByUser() : Returntype {
        return $this->getEntityManager()
        ->createQuery('SELECT '
    } */
    
    public function removeOne($id, $owner_id) : array {
        return $this->getEntityManager()
        ->createQuery(
            'DELETE f FROM App\Entity\File f
           WHERE f.id = :id 
           and IDENTITY(f.owner) = :owner_id'
        )
        ->setParameter('id', $id) 
        ->setParameter('owner_id', $owner_id) 
        ->getResult();
        
    }

    //once uset to debug, remove it when you're ready
    /* public function findNothing()  {
        return $this->getEntityManager()
        ->createQuery(
            'SELECT f.name FROM App\Entity\File f
            WHERE IDENTITY(f.owner) = 11'

        )->getResult();
    } */

    public function findOneById($val) : array {
        return $this->getEntityManager()
        ->createQuery(
            'SELECT f.name, f.tag FROM App\Entity\File f
            WHERE f.id = :val'
        )
        ->setParameter('val', $val) 
        ->getResult();
    }

    /* public function () : Returntype {
        
    } */

}
