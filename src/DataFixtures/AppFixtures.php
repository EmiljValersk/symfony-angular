<?php

namespace App\DataFixtures;

// use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\File;
use DateTimeImmutable;

class AppFixtures extends Fixture
{
    private $hasher;
   
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $names = ["Anton", "Ludwig", "Emilija", "Kathrin", "Frank"];
        $surnames = ["Rot", "Martin", "Ewerty", "Majoblu", "Awej"];
        $emails = ["anton@rot.co", "ludwig@ludi.co", "emily@emi.lya", "katrin@kat.rin", "frank@frank.co"];
        $passwords = ["antonpass", "ludwigpass", "emilijapass", "kathrinpass", "frankpass"];
        $fileTypes = ["Video", "Audio", "Pdf","mscx" ];
        $tags = ["music", "mathematic", "languages", "neuroscience", "art"];

        for($i = 0; $i <5; $i++){
            $user = new User();
            $user->setName($names[$i]) ;
            $user->setSurname($surnames[$i]);
            $user->setEmail($emails[$i]);
            $user->setPassword($this->hasher->hashPassword($user, $passwords[$i]));
            $manager->persist($user);

            for($a = 0; $a < count($names); $a++ ) {
                $pdf = new File;
                $date = new DateTimeImmutable();
                $pdf->setName("The work of Author".$a);
                $pdf->setSize(5454);
                $pdf->setType($fileTypes[2]);
                $pdf->setOwner($user);
                $tag = $tags[array_rand($tags, 1)];
                $pdf->setTag($tag);
                $pdf->setCreatedAt($date);
                $manager->persist($pdf);
            }

            for($b = 0; $b < count($names); $b++ ) {
                $video = new File;
                $date = new DateTimeImmutable();
                $video->setName("Video from user". $b);
                $video->setType($fileTypes[0]);
                $video->setSize(554);
                $video->setCreatedAt($date);
                $tag = $tags[array_rand($tags, 1)];
                $video->setTag($tag);
                $video->setOwner($user);
                $manager->persist($video);
        
            }
        }

        $manager->flush();
    }
}
