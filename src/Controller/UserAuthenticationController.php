<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;


class UserAuthenticationController extends AbstractController
{

    //TO-DO: replace the dummy-frontend code below with your (TO-DO, as well) angular code in frontend


    #[Route("/login", name: "login", methods: ['GET', 'POST'])]
    public function login(): Response
    {
        return $this->render("dummy-frontend/login.html.twig");
    }


    #[Route('/authentication', name: 'app_user_authentication')]
    public function authenticate(
        #[CurrentUser] ?User $user,
        Request $request,
        JWTTokenManagerInterface $JWTManager
    ) {

        if (null === $user) {
            return $this->json([
                'message' => 'missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $authenticatedUser = $user->getUserIdentifier();
        $token = $JWTManager->create($user);
        
        return $this->json([
            'token' => $token,
            
        ]);


        // TO-DO: now we have a token created on the backend, 
        //   so let's add it to Requests outgoing from the frontend

    }

    #[Route("/logout", name: "logout")]
    public function logOut(Security $security): Response
    {
        return new Response($security->logout());
    }
}
