<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\File;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

 #[IsGranted('IS_AUTHENTICATED_FULLY')]

class FileController extends AbstractController
{

    /* TO-DO:
    
    Add methods and endpoints for:
    - upload file, list files, get file details, update file, delete
    - each user can only perform actions on his own files, can see all files.
    - User has to be logged in --> endpoints for login (all users), add/remove/edit user (user can only perform
      it on his profile)  */

    #[Route("/files", methods: "GET", format: "json")]
    public function listFiles(EntityManagerInterface $manager, SerializerInterface $serializer): Response
    {
        $files = $manager->getRepository(File::class)->findAllNamesAndTags();
        return new Response($serializer->serialize($files, 'json'));
    }

    #[Route("/file/{id}", name: "file_details", methods: "GET", format: "json")]
    public function getFile(
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        int $id
    ): Response {
        $file = $manager->getRepository(File::class)->findOneById(array('id' => $id));
        return new Response($serializer->serialize($file, 'json'));
        
    }

    #[Route("/files/{id}/details", methods: "GET", format: "json")]
    public function getFileDetails(
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        int $id
    ): Response {
        $file = $manager->getRepository(File::class)->find($id);
        return new Response($serializer->serialize($file, 'json', [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function (object $object) {
                return $object->getId();
            }
        ]));
       
    }
    #[Route("/files/{tag}", name: "app_files", methods: "GET", format: "json")]
    public function getFilesByTag(
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        string $tag
    ): Response {

        $files = $manager->getRepository(File::class)->findByTag(array('tag' => $tag));
        
        /*  return new Response($serializer->serialize($files,'json', [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function (object $object) {
            return $object->getId(); 
            
            // ! be careful this will expose even user's passwords if you don't limit the fields to select in your query! 
        }
      ]
     )); */
        return new Response($serializer->serialize($files, 'json'));
    }


    #[Route("files/{id}/delete", methods: ["GET", "DELETE"])]
    public function deleteFile(EntityManagerInterface $manager, int $id, int $owner_id, ?User $user): Response
    {
        $file = $manager->getRepository(File::class)->find($id);
    
        //TO-DO: add some error handling logic

        if ($file->getOwner() == $user->getId()) {
            $manager->getRepository(File::class)->removeOne([array('id' => $id, 'owner_id' => $owner_id)]);
            $manager->flush();
            return $this->json(['message' => 'file successfully deleted']);
        }
    }
    #[Route("/files/add", methods: ["POST"])]
    public function uploadFile(
        EntityManagerInterface $manager,
        Request $request,
        $name,
        $owner,
        $type,
        $tag,
        $size,
        $createdAt
    ): Response {
       
        // $postData = json_decode($request->getContent(), true); //you will get the values from the request sent through the frontend
        
        $file = new File();
        $file->setName($name);
        $file->setOwner($owner);
        $file->setType($type);
        $file->setTag($tag);
        $file->setSize($size);
        $file->setCreatedAt($createdAt);

        $manager->persist($file);
        $manager->flush();

        return new Response("file successfully uploaded");
    }
}
