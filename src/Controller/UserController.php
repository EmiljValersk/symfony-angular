<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\File;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


#[IsGranted('IS_AUTHENTICATED_FULLY')]
class UserController extends AbstractController
{

    #[Route("/users", name: "users", methods: "GET", format: "json") ]
    public function listUsers(EntityManagerInterface $manager, SerializerInterface $serializer) {
        
        // the method below: a query which selects only name and surname of users
        // this comes with no circular-reference issue, if you don't want to retrieve also all associated
        // user's files already at the beginning

        $users = $manager->getRepository(User::class)->findyByFullName();
        return new Response($serializer->serialize($users,'json'));
         
       
        //if you call findAll() then you have to handle the circular-reference because of the association 
        //with files
       
    }

    #[Route("/user/{surname}", name: "user", methods: "GET", format: "json")]
    public function findUsername(EntityManagerInterface $manager, SerializerInterface $serializer, string $name)  {
        $user = $manager->getRepository(User::class)->findUserByFullName(array('surname' => $name ));
        
        return new Response($serializer->serialize($user,'json'));
        
    }

    #[Route("/user/{owner_id}/files", methods: "GET", format: "json")]
    public function getUserFiles(EntityManagerInterface $manager,
    SerializerInterface $serializer,
    int $owner_id) : Response {
        $files = $manager->getRepository(File::class)->findByUserId(array('owner_id' => $owner_id));
        return new Response($serializer->serialize($files, 'json'));
    }
    
    #[Route("user/{id}/full", name: "user_full_details", methods: "GET", format: "json")]
    
    /* in this route we retrieve all user details including the associated files,
    so we will have to handle the circular reference */
    
    public function userDetails(EntityManagerInterface $manager, 
    SerializerInterface $serializer, 
    int $id) : Response 
    {
        $user = $manager->getRepository(User::class)->findOneById(array('id' => $id));
       
        return new Response($serializer->serialize($user, 'json', [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function (object $object) {
                return $object->getId();
                // same here I just expected the username, but the json is including also all the files.
                // be careful this will expose user's password if you don't limit the fields to select in your query!
            },
        ]
        ));
    }

   


}
