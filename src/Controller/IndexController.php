<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController
{
    #[Route("/index", name: "index")]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        return $this->render('dummy-frontend/user-actions.html.twig', ['user' => $user]);
        
        // above(TO-DO): create some json endpoints for the user e.g. profile, files, CRUD operations
        // and some links to them: your profile, your files, all files, other user's profiles.
                        //   your profile->edit/delete, your files->add file, get single file->edit/delete
    }

    #[Route("/testurl", name: "testurl", methods: ['GET', 'POST'])]
    public function hallo(Request $request)  {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        return new JsonResponse([
            'user' => $user]);
    }


}
