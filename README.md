# Symfony + Angular Beispielprojekt

## (German)
Es handelt sich hier um ein einfaches, kleines Projekt um mit Symfony im Backend und Angular im Frontend zu arbeiten.

Einfache, klassische CRUD Anwendung, Nutzer Login/Logout, eigene Dateien hochladen zur Zwecke der Wissen-sharing.

Das Symfony Backend stellt Json endpoints brereit fur Create, Read, Update und Delete Operationen.

Bisher: fiktive Anwendung ohne real-world use-case, dient der eigene Weiterbildung.

Die Idee: Nutzer, die gerne ihr Know-how teilen und austauschen, können eigene Dateien hochladen (PDF und/oder Videos eigener Herstellung - Achtung Copyright- , Tags: z.B. Naturwissenschaften,  MINT-Fächer, Geschichte, Musik, Psychologie, usw. ), Dateien von anderen Nutzern ansehen, ihre eigene Dateien ändern, löschen, mit anderen Nutzer Privatnachrichten tauschen, Events organizieren und/oder daran teilnehmen.

Gerade angefangen und noch fast alles TO-DO, zur Zeit ist noch ein Dummy- Twig Frontend vorhanden, was in absehbarer Zeit durch Angular ersetzt wird.
## (English)
Just a small and little projekt to connect a Symfony Backend and an Angular frontend.

Simple, classic CRUD application, users can login/logout, upload some own files to share knowledge with other users (PDF and/or video, with some tags, e.g Informatic, Science, Music, Psychology, etcetera, no Copyright-protected stuff), communicate with other users via PM, attend events, & maybe more.

The Backend and ant the Frontend will comunicate via Json endpoints.

Until now no real-world use-case, just fictional stuff to keep on learning with Angular and Symfony,  the Idea is just Know-how sharing.

The project has just been started and still almost everything TO-DO, at moment there is still a dummy- Twig frontend, to be replaced with Angular.

