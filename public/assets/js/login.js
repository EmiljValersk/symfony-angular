/******************************************
 *  Author : Author   
 *  Created On : Tue Apr 23 2024
 *  File : login.js
 *******************************************/

window.onload = function () {
  const loginForm = document.getElementById("loginForm")
  const name = document.getElementById("name")
  const pass = document.getElementById("password")

  loginForm.onsubmit = async (e) => {
    e.preventDefault();
    const data = new FormData(loginForm)
    data['email'] = name.value
    data['password'] = password.value
    const str = JSON.stringify(data) 
    // console.log(str); return; //{"email":"ab@nton.de","password":"regrf"} --> works
    const response = await fetch("/authentication", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: str
      
    });
    window.location.href="/index"

  }
}