import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { AuthorizationComponent } from './authorization/authorization.component';
import { DummyComponent } from './dummy/dummy.component';

const routes: Routes = [
  { path: 'login', component: AuthorizationComponent },
  { path: 'welcome', component: WelcomeScreenComponent},
  { path: 'dummy', component: DummyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
