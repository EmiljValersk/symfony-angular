import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthorizationComponent } from './authorization/authorization.component';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { Globals } from './globals';

import { TokenService } from './token.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthorizationComponent,
    WelcomeScreenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [Globals,
    {provide: HTTP_INTERCEPTORS, useClass: TokenService, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


