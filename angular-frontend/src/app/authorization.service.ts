import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Globals } from './globals';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private http: HttpClient, private globals: Globals, ) { }

 
  // eventually refactor the login-logic in the authorization-component to this service, or remove it if not used.
  
  login(postData:{email: string; password: string} ) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'})
      
      this.http.post<any>(this.globals.APIURL+'authentication',
      postData, { headers }) 
      .subscribe(data => {console.log(data)}
                
    )
      
    
   }

}
