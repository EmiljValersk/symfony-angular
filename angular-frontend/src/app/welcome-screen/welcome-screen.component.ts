import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';

@Component({
  selector: 'app-welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrl: './welcome-screen.component.css'
})
export class WelcomeScreenComponent implements OnInit {

  constructor(private http: HttpClient, private globals: Globals) { }
  
  ngOnInit() {
    this.getData();
  }

  private getData() {
    
      this.http.get(this.globals.APIURL + 'testurl')
      .subscribe(data => console.log(data));
  }

}
