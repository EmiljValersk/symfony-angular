import { HttpInterceptor } from '@angular/common/http';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService implements HttpInterceptor{

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    
    let jwt = localStorage.getItem('jwt'); console.log("token: ", jwt)
    if(jwt != null){
      let token = JSON.parse(jwt)
      request = request.clone({
        setHeaders: {
            Authorization: `Bearer ${token}`
        }
    });
    }
    return next.handle(request);
}
}
