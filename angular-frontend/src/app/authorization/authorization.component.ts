import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Globals } from '../globals';
import { AuthorizationService } from '../authorization.service'
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrl: './authorization.component.css',

  
})
export class AuthorizationComponent {



  constructor(private http: HttpClient, private globals: Globals, private router: Router ) { }
    
     onSubmit(postData: { email: string; password: string }) {
    // Send Http request
    this.http
      .post(this.globals.APIURL+'authentication',
        postData
      ).pipe(
        map(response => {
            if (response) {
                localStorage.setItem('jwt', JSON.stringify(response));
            }
        })
      ).subscribe(responseData => {
        console.log(responseData); 
        this.router.navigate(['welcome']);
      });
     
  }
  
  // TO-DO: retrieve session cookie, or implement some JWT to let the server know about the logged-in user,
  // reset form, redirect user to some nice startpage where they can access resources, etcetera.
   
  // eventually, refactor this logic to the AuthorizationService (or remove it if you don't use it)  
  


}
